# zorilla

## Installation
### RHEL 7
#### Pre-requisites
Install python 3.6+ and mod_wgsi 4.5+ from the RedHat Software Collections (SCL):

    sudo yum install gcc mariadb-devel rh-python36-mod_wsgi httpd24-mod_ssl rh-python36-python-virtualenv

Next, create a system account for mod_wsgi to run the application as:

    sudo useradd --home-dir /srv/zorilla --system zorilla -s /sbin/nologin

#### Apache/Shibboleth
Installation and setup of mod_shib is beyond the scope of this document.

Create a virtualhost config file in `/opt/rh/httpd24/root/etc/httpd/conf.d/`:

    LoadModule mod_shib /usr/lib64/shibboleth/mod_shib_24.so
    <VirtualHost *:80>
       ServerName zorilla.example.com
       Redirect permanent / https://zorilla.example.com/
    </VirtualHost>
    <VirtualHost *:443>
      ServerName zorilla.example.com
      ServerSignature Off
      ProxyPreserveHost On
      RedirectMatch ^/$ /web/
      # Use v1 API by default
      RewriteEngine on
      RewriteCond "%{REQUEST_URI}" "!^/api/v[0-9.]+/"
      RewriteRule   "^/api/(.+)?" "/api/v1/$1" [R,L]
      WSGIProcessGroup zorilla
      WSGIDaemonProcess zorilla python-path=/srv/zorilla/zorilla/:/srv/zorilla/env/lib/python3.6/site-packages
      WSGIScriptAlias / /srv/zorilla/zorilla/zorilla/wsgi.py process-group=zorilla application-group=%{GLOBAL}
      WSGIPassAuthorization On
      Alias /static /srv/zorilla/static
      <Directory /srv/zorilla/static>
        Require all granted
      </Directory>
      <Directory /srv/zorilla/zorilla/zorilla/wsgi.py>
        <Files wsgi.py>
          Require all granted
        </Files>
      </Directory>
      <Location />
        AuthType shibboleth
        Require shibboleth
      </Location>
      SSLEngine On
      SSLCertificateFile /etc/pki/tls/certs/example.cer
      SSLCertificateKeyFile /etc/pki/tls/private/example.key
      SSLCertificateChainFile /etc/pki/tls/certs/example-chain.cer
    </VirtualHost>


#### MySQL/MariaDB
    sudo yum install mariadb-server
Create a new database `zorilla` and a user with `ALL PRIVILEGES ON zorilla.*`

#### RabbitMQ
  - Install the `rabbitmq-server` package: https://www.rabbitmq.com/install-rpm.html
  - Configure to use SSL/TLS on port 5671
  - Create a `zorilla` user with full permissions to the default vhost

#### Application
    cd /srv
    sudo git clone https://gitlab.uvm.edu/saa/research-storage-automation/zorilla.git .
    chmod 750 /srv/zorilla
    sudo chown -R zorilla.apache /srv/zorilla
    sudo su -l zorilla -s /bin/bash
    scl enable rh-python36 bash
    virtualenv env
    source env/bin/activate
    pip install -r requirements.txt
    cd zorilla

Create the file `/srv/zorilla/zorilla/zorilla/local_settings.py` and customize the contents

    ALLOWED_GROUPS = ["somegroup", "someothergroup"]
    ALLOWED_HOSTS = ["*"]
    APP_NAME = "Storage Manager"
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.mysql",
            "NAME": "zorilla",
            "USER": "zorilla",
            "PASSWORD": "secretsecretsecret",
            "HOST": "database.example.com",
            "PORT": "3306",
        }
    }
    DEFAULT_FILESYSTEM_REFQUOTA = 5497558138880.0
    DEFAULT_FILESYSTEM_QUOTA = DEFAULT_FILESYSTEM_REFQUOTA * 1.2
    DEFAULT_SNAPSHOT_RETENTION = 30
    DEFAULT_ZPOOL_NAME = "tank"
    LDAP_SERVER = "ldap.example.com"
    LDAP_USER_SEARCH_BASE = "ou=People,dc=example,dc=com"
    LDAP_GROUP_SEARCH_BASE = "ou=Groups,dc=example,dc=com"
    RABBITMQ_SERVER = "rabbitmq.example.com"
    RABBITMQ_PORT = 5671
    RABBITMQ_TLS = True
    RABBITMQ_USER = "zorilla"
    RABBITMQ_PASSWORD = "secretsecretsecret"
    RABBITMQ_VHOST = "/"
    SHIBBOLETH_ATTRIBUTE_MAP = {
        "eppn": (True, "username"),
        "givenName": (False, "first_name"),
        "surname": (False, "last_name"),
        "mail": (True, "email"),   
        "cn": (False, "full_name"),
        "uid": (False, "external_uid"),
    }
    STATIC_ROOT = "/srv/zorilla/static/"
    SNAPSHOT_RETENTION_VALUES = [0, 30, 90]

Finally, install the database schema and copy static files:

    ./manage.py migrate
    ./manage.py collectstatic --no-input
    ./manage.py check

Create a superuser account for the admin interface:

    ./manage.py createsuperuser

FIN
