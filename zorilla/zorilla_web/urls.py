from django.urls import path, include
from django.contrib.auth.decorators import login_required
from jchart.views import ChartView
from zorilla_web.charts import FsQuotaChart
from zorilla_web.views import *

app_name = "zorilla-web"

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("dashboard/", DashboardView.as_view(), name="dashboard"),
    path("user/profile/", UserProfileView.as_view(), name="user_profile"),
    path(
        "filesystem/<int:fs_id>/edit/",
        FilesystemEditView.as_view(),
        name="filesystem_edit",
    ),
    path(
        "charts/fs_quota/<int:fs_id>/",
        login_required(ChartView.from_chart(FsQuotaChart())),
        name="fs_quota_chart",
    ),
]
