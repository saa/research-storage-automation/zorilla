from django import template
from zorilla_core.functions import lookup_ldap_by_gid, lookup_ldap_by_uidnumber

register = template.Library()


@register.filter()
def ldap_user_name(uidnumber):
    """
    Returns a user's name given its numeric uid
    """
    user = lookup_ldap_by_uidnumber(uidnumber, attributes=["uid"])
    if user is not None:
        return user.uid


@register.filter()
def ldap_group_name(gid):
    """
    Returns a group's cn given its gid
    """
    group = lookup_ldap_by_gid(gid, attributes=["cn"])
    if group is None:
        return "nobody"
    else:
        return group.cn
