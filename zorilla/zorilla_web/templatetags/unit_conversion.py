from django import template

register = template.Library()


@register.filter
def bytes_to_kbytes(value):
    return value / 1024


@register.filter
def bytes_to_mbytes(value):
    return value / 1024 ** 2


@register.filter
def bytes_to_gbytes(value):
    return value / 1024 ** 3


@register.filter
def bytes_to_tbytes(value):
    return value / 1024 ** 4
