from django.conf import settings


def app_name(request):
    """
    Adds APP_NAME from settings.py to the context
    """
    return {"app_name": settings.APP_NAME}


def logo_url(request):
    """
    Adds LOGO_URL from settings.py to the context
    """
    return {"logo_url": settings.LOGO_URL}
