from django.apps import AppConfig


class ZorillaWebConfig(AppConfig):
    name = "zorilla_web"
