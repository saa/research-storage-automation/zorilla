from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext
from django.views.generic import TemplateView, View
from django.views.generic.edit import FormView
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from zorilla_core.models import ZFilesystem, ZSnapshot
from zorilla_web.functions import (
    get_fsid_from_path,
    get_group_id_choices,
    get_filesystem,
    get_filesystem_form,
    get_self_provision_form,
    get_user_filesystems,
    provision_filesystem,
    provision_group_change,
    provision_backup,
    set_profile,
)
from zorilla_web.forms import SelfProvisionForm, ZFilesystemForm


class IndexView(TemplateView):
    def page_title(self):
        """
        Render page title for template
        """
        return ugettext("Main")

    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect("web:dashboard")
        else:
            return render(
                self.request,
                template_name="zorilla_web/index.html",
                context={"view": self},
            )


@method_decorator(never_cache, name="dispatch")
class DashboardView(LoginRequiredMixin, TemplateView):
    def page_title(self):
        """
        Render page title for template
        """
        return ugettext("Dashboard")

    def filesystems(self):
        """
        Render filesystem list for template
        """
        return get_user_filesystems(self.request.user)

    def get(self, *args, **kwargs):
        form = get_self_provision_form(self.request.user)
        return render(
            self.request,
            template_name="zorilla_web/dashboard.html",
            context={"form": form, "view": self},
        )

    def post(self, request, **kwargs):
        form = SelfProvisionForm(request.POST)
        form.fields["group_id"].choices = get_group_id_choices(
            request.user, initial_choice=None
        )
        if form.is_valid():
            if request.user.userprofile.self_provisioned == False:
                provision_filesystem(request.user, form.cleaned_data)
                set_profile(request.user, "self_provisioned", True)
                messages.success(
                    request,
                    ugettext(
                        "Your share is being provisioned. Please reload this page in a few minutes."
                    ),
                )
            else:
                messages.error(
                    request, ugettext("The user share was already provisioned")
                )
        else:
            messages.error(request, form.errors)
        return redirect(reverse("web:dashboard"))


class FilesystemEditView(LoginRequiredMixin, View):
    def page_title(self):
        return ugettext("Edit Filesystem")

    def get(self, *args, **kwargs):
        filesystem = get_filesystem(kwargs["fs_id"])
        form = get_filesystem_form(self.request.user, filesystem)
        return render(
            self.request,
            template_name="zorilla_web/filesystem_edit.html",
            context={"filesystem": filesystem, "form": form, "view": self},
        )

    def post(self, request, **kwargs):
        form = ZFilesystemForm(request.POST)
        filesystem_id = get_fsid_from_path(request.path)
        filesystem = get_filesystem(filesystem_id)
        form.fields["group_id"].choices = get_group_id_choices(
            request.user, initial_choice=filesystem.group_id
        )
        if form.is_valid():
            provision_group_change(filesystem_id, form.cleaned_data)
            provision_backup(self.request.user, filesystem_id, form.cleaned_data)
            messages.success(request, ugettext("Changes submitted!"))
            return redirect(reverse("web:dashboard"))
        else:
            messages.error(request, form.errors)
            return redirect("web:filesystem_edit", fs_id=kwargs["fs_id"])


class UserProfileView(LoginRequiredMixin, TemplateView):
    template_name = "zorilla_web/user_profile.html"

    def page_title(self):
        """
        Render page title for template
        """
        return ugettext("Profile")
