from django import forms
from django.conf import settings
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy
from zorilla_web.validators import validate_snapshot_retention, validate_filesystem_name


class ZFilesystemForm(forms.Form):
    group_id = forms.ChoiceField(
        label=ugettext_lazy("Group access"),
        help_text=ugettext_lazy("Members of this group will have access to the share."),
    )
    snapshot_retention = forms.ChoiceField(
        label=ugettext_lazy("Snapshot retention days"),
        help_text="{} {}".format(
            ugettext_lazy("Number of days to keep backup snapshots."),
            ugettext_lazy("Space used by snapshots counts against your overall quota."),
        ),
        choices=[(c, c) for c in settings.SNAPSHOT_RETENTION_VALUES],
        validators=[validate_snapshot_retention],
    )


class SelfProvisionForm(forms.Form):
    filesystem_name = forms.CharField(
        label=ugettext_lazy("Share name"),
        help_text=ugettext_lazy(
            "The name must contain only lowercase alphanumeric characters ('a-z0-9'), hyphens ('-'), and underscores ('_')"
        ),
        widget=forms.TextInput(attrs={"id": "filesystem_name"}),
        validators=[
            RegexValidator(
                regex="^[a-z0-9_-]+$", message=ugettext_lazy("Invalid characters")
            ),
            validate_filesystem_name,
        ],
    )
    group_id = forms.ChoiceField(
        label=ugettext_lazy("Group access"),
        help_text=ugettext_lazy("Members of this group will have access to the share."),
    )
    share_smb = forms.BooleanField(
        initial=True, required=False, widget=forms.HiddenInput()
    )
    share_nfs = forms.BooleanField(
        initial=False, required=False, widget=forms.HiddenInput()
    )
