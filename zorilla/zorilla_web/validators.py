from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy
from zorilla_core.models import ZFilesystem


def validate_snapshot_retention(value):
    """
    Only allow values in SNAPSHOT_RETENTION_VALUES
    """
    list = settings.SNAPSHOT_RETENTION_VALUES
    if int(value) not in list:
        raise ValidationError(
            ugettext_lazy(
                "The following values are permitted: {}".format(sorted(list))
            ),
            params={"value", value},
        )


def validate_filesystem_name(value):
    """
    Check for existence of the given fsname
    """
    if ZFilesystem.objects.filter(fsname=value):
        raise ValidationError(ugettext_lazy("This name is already in use"))
