from jchart import Chart
from jchart.config import DataSet
from django.conf import settings
from django.utils.translation import ugettext
from zorilla_core.models import ZFilesystem


class DonutChart(Chart):
    """
    Base class for donut charts
    """

    chart_type = "doughnut"
    responsive = True


class FsQuotaChart(DonutChart):
    """
    Display a filesystem's utilization as a donut chart
    """

    def get_datasets(self, fs_id):
        try:
            fs = ZFilesystem.objects.get(id=fs_id)
            used = round((fs.used_bytes) / 1024 ** 3, 2)
            free = round((fs.refquota_bytes - fs.used_bytes) / 1024 ** 3, 2)
            colors = settings.CHART_COLORS
            hover_colors = colors.copy().reverse()
            return [
                DataSet(
                    label=fs.fsname,
                    data=[used, free],
                    backgroundColor=colors,
                    hoverBackgroundColor=hover_colors,
                )
            ]
        except ZFilesystem.DoesNotExist:
            pass

    def get_labels(self, **kwargs):
        """
        Return translated strings for chart labels
        """
        return [ugettext("Used"), ugettext("Free")]
