import json, logging, re, secrets
from django.conf import settings
from zorilla_core.models import UserProfile, ZFilesystem, ZSnapshot
from zorilla_web.forms import SelfProvisionForm, ZFilesystemForm
from zorilla_core.functions import (
    lookup_ldap_by_uid,
    lookup_ldap_by_gid,
    lookup_ldap_groups,
    mq_connect,
    mq_publish,
)

logger = logging.getLogger("zorilla.zorilla_web")


def get_filesystem(id):
    """
    Return a ZFilesystem object matching id or None
    """
    try:
        return ZFilesystem.objects.filter(id=id).first()
    except ZFilesystem.DoesNotExist:
        pass


def get_grouplist(user):
    """
    Return a list of user's LDAP groups
    """
    return [cn for gid, cn in get_group_id_choices(user, initial_choice=None)]


def get_user_filesystems(user):
    """
    Return a user's ZFilesystem objects or None
    """
    ldap_user = lookup_ldap_by_uid(
        user.userprofile.external_uid, attributes="uidNumber"
    )
    try:
        filesystems = ZFilesystem.objects.filter(owner_id=ldap_user.uidNumber).values()
        for fs in filesystems:
            fs["snapshots"] = ZSnapshot.objects.filter(filesystem_id=fs["id"]).order_by(
                "-name"
            )
        return filesystems
    except (ZFilesystem.DoesNotExist, ZSnapshot.DoesNotExist):
        pass


def get_filesystem_form(user, filesystem):
    """
    Return a ZFilesystemForm populated with initial values
    """
    form = ZFilesystemForm()
    form.fields["group_id"].choices = get_group_id_choices(user, filesystem.group_id)
    form.fields["group_id"].initial = filesystem.group_id
    form.fields["snapshot_retention"].initial = filesystem.snapshot_retention
    return form


def get_self_provision_form(user):
    """
    Return a SelfProvisionForm populated with initial values
    """
    form = SelfProvisionForm()
    form.fields["filesystem_name"].initial = user.userprofile.external_uid
    form.fields["group_id"].choices = get_group_id_choices(user, None)
    form.fields["group_id"].initial = "99"
    return form


def get_group_id_choices(user, initial_choice=None):
    """
    Return a list of tuples for group selector fields
    """
    # get the user's LDAP group memberships
    choices = []
    for g in lookup_ldap_groups(
        user.userprofile.external_uid, attributes=["cn", "gidNumber"]
    ):
        gid = str(g.gidNumber)
        # sometimes g.cn looks like "['group', 'some description']" instead of "group"
        m = re.match(r"\[\'([^'\]]+)", str(g.cn))
        if m:
            cn = m.group(1)
        else:
            cn = str(g.cn)
        choices.append((gid, cn))
    choices.append(("99", "nobody"))
    if initial_choice is None:
        current_choice = ("99", "nobody")
    else:
        ldap_group = lookup_ldap_by_gid(initial_choice, attributes=["cn"])
        current_choice = (
            initial_choice,
            "nobody" if ldap_group is None else str(ldap_group.cn),
        )
        # if the filesystem's group_id is not in the list of choices, add it
    if current_choice not in choices:
        choices.append(current_choice)
    # sort by group cn
    return sorted(choices, key=lambda c: str(c[1]))


def get_fsid_from_path(url_path):
    """
    Parse the filesystem id from the request path
    """
    m = re.search("\/filesystem\/([0-9]+)\/", url_path)
    if m:
        return int(m.group(1))


def get_profile(user):
    """
    Retreive a user UserProfile
    """
    try:
        profile = UserProfile.objects.get(user=user)
        return profile
    except UserProfile.DoesNotExist:
        pass


def set_profile(user, field, value):
    """
    Set a UserProfile attribute
    """
    try:
        profile = UserProfile.objects.get(user=user)
        setattr(profile, field, value)
        profile.save()
        return True
    except UserProfile.DoesNotExist:
        return False


def provision_group_change(filesystem_id, data):
    """
    Send a group_change command
    """
    payload = {"filesystem_id": filesystem_id, "group_id": data["group_id"]}
    logger.debug("Publishing to group_change queue: {}".format(json.dumps(payload)))
    mq_publish(payload, routing_key="group_change")


def provision_backup(user, filesystem_id, data):
    """
    Send a provision_backup command
    """
    payload = {
        "external_uid": user.userprofile.external_uid,
        "filesystem_id": filesystem_id,
        "retention_days": int(data["snapshot_retention"]),
    }
    logger.debug("Publishing to provision_backup queue: {}".format(json.dumps(payload)))
    mq_publish(payload, routing_key="provision_backup")


def provision_filesystem(user, data):
    """
    Send a provision_filesystem command
    """
    ldap_user = lookup_ldap_by_uid(
        user.userprofile.external_uid, attributes="uidNumber"
    )
    payload = {
        "filesystem_name": data["filesystem_name"],
        "owner_id": str(ldap_user.uidNumber),
        "group_id": data["group_id"],
        "quota": int(settings.DEFAULT_FILESYSTEM_QUOTA),
        "refquota": int(settings.DEFAULT_FILESYSTEM_REFQUOTA),
        "zpool": settings.DEFAULT_ZPOOL_NAME,
        "external_uid": user.userprofile.external_uid,
        "sharesmb": data["share_smb"],
        "sharenfs": data["share_nfs"],
    }
    logger.debug(
        "Publishing to provision_filesystem queue: {}".format(json.dumps(payload))
    )
    mq_publish(payload, routing_key="provision_filesystem")
