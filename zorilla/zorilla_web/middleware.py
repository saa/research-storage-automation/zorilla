import logging
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.urls import resolve
from shibboleth.middleware import ShibbolethRemoteUserMiddleware
from django.utils.translation import ugettext
from zorilla_core.models import UserProfile
from zorilla_web.functions import get_grouplist

logger = logging.getLogger("zorilla.zorilla_web")


class CustomShibbolethRemoteUserMiddleware(ShibbolethRemoteUserMiddleware):
    """
    Subclass of ShibbolethRemoteUserMiddleware to define make_profile, etc
    """

    def make_profile(self, user, shib_meta):
        """
        Create and populate a UserProfile object
        """
        if not hasattr(user, "userprofile"):
            profile = UserProfile(user=user)
            if "full_name" in shib_meta.keys():
                profile.full_name = shib_meta["full_name"]
            if "external_uid" in shib_meta.keys():
                profile.external_uid = shib_meta["external_uid"]
            profile.save()
            logger.debug(
                "Created user profile for {}: {}".format(
                    user.id, getattr(profile, "external_uid", "")
                )
            )


class GroupAccessMiddleware:
    """
    Only permit members of ALLOWED_GROUPS to access zorilla-web
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def check_groups(self, user):
        """
        Check if user is a member of ALLOWED_GROUPS from ldap
        """
        if "__all__" in settings.ALLOWED_GROUPS:
            return True
        grouplist = get_grouplist(user)
        return len(set(settings.ALLOWED_GROUPS).intersection(grouplist)) > 0

    def __call__(self, request):
        """
        Perform group access comparison
        """
        if resolve(request.path).app_name == "zorilla-web":
            if request.user.is_authenticated:
                if self.check_groups(request.user):
                    # set group_checked flag on user
                    setattr(request.user, "groups_checked", True)
                else:
                    logger.debug(
                        "User {} denied access: Not member of ALLOWED_GROUPS".format(
                            getattr(request.user.userprofile, "external_uid", "")
                        )
                    )
                    raise PermissionDenied
        return self.get_response(request)
