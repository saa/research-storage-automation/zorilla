from django.conf import settings
from django.test import RequestFactory, TestCase
from django.contrib.auth.models import AnonymousUser, User
from django.db.models.query import QuerySet
from unittest import mock
from zorilla_web.views import *
from zorilla_core.models import *


class FakeLDAPUser:
    def __init__(self):
        self.uidNumber = 42


class ClassViewTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username="test", email="test@example.com", password="test"
        )
        setattr(self.user, "profile", UserProfile(user=self.user))
        self.extraSetUp()

    def extraSetUp(self):
        """
        Sub-classes can do more setup here
        """
        pass

    @mock.patch("zorilla_core.functions.ldap_connection")
    def get(
        self, cls, user, url, mock_ldap, status_code=200, redirect_url=None, kwargs={}
    ):
        request = self.factory.get(url)
        request.user = user
        response = cls.as_view()(request, **kwargs)
        self.assertEqual(response.status_code, status_code)
        if redirect_url is not None:
            self.assertEqual(response.url, redirect_url)
        return response

    def get_anonymous(self, cls, url, status_code=302, redirect_url=None, kwargs={}):
        if redirect_url is None:
            redirect_url = f"/Shibboleth.sso/Login?next={url}"
        return self.get(
            cls,
            AnonymousUser(),
            url,
            status_code=status_code,
            redirect_url=redirect_url,
        )

    def get_authenticated(
        self, cls, url, status_code=200, redirect_url=None, kwargs={}
    ):
        return self.get(
            cls,
            self.user,
            url,
            status_code=status_code,
            redirect_url=redirect_url,
            kwargs=kwargs,
        )


class IndexViewTestCase(ClassViewTestCase):
    def test_get_anonymous(self):
        self.get(IndexView, AnonymousUser(), "/")

    def test_get_authenticated(self):
        self.get_authenticated(
            IndexView, "/", status_code=302, redirect_url="/web/dashboard/"
        )


class DashboardViewTestCase(ClassViewTestCase):
    def test_get_anonymous(self):
        self.get_anonymous(DashboardView, "/web/dashboard/")

    def test_get_authenticated(self):
        self.get_authenticated(DashboardView, "/web/dashboard/")

    def test_page_title(self):
        self.assertEqual(DashboardView().page_title(), "Dashboard")

    @mock.patch("zorilla_web.functions.lookup_ldap_by_uid")
    def test_filesystems(self, mock_lookup):
        ldap_user = FakeLDAPUser()
        mock_lookup.return_value = ldap_user
        ZFilesystem.objects.create(fsname="test", owner_id=ldap_user.uidNumber)
        request = self.factory.get("/web/dashboard/")
        setattr(request, "user", self.user)
        view = DashboardView(request=request)
        filesystems = view.filesystems()
        self.assertIsInstance(filesystems, QuerySet)
        filesystem = filesystems.first()
        self.assertEqual(filesystem["fsname"], "test")
        self.assertEqual(int(filesystem["owner_id"]), ldap_user.uidNumber)


class FilesystemEditViewTestCase(ClassViewTestCase):
    def extraSetUp(self):
        self.filesystem = ZFilesystem(
            owner_id=self.user.id,
            group_id=0,
            fsname="fsname",
            zpool="zpool",
            hostname="example.com",
        )
        self.filesystem.save()

    def test_get_anonymous(self):
        self.get_anonymous(FilesystemEditView, "/web/filesystem/1/edit/")

    def test_get_authenticated(self):
        self.get_authenticated(
            FilesystemEditView,
            f"/filesystem/{self.filesystem.id}/edit/",
            kwargs={"fs_id": self.filesystem.id},
        )

    def test_page_title(self):
        self.assertEqual(FilesystemEditView().page_title(), "Edit Filesystem")


class UserProfileViewTestCase(ClassViewTestCase):
    def test_get_anonymous(self):
        self.get_anonymous(UserProfileView, "/web/profile/")

    def test_get_authenticated(self):
        self.get_authenticated(UserProfileView, "/web/profile/")

    def test_page_title(self):
        self.assertEqual(UserProfileView().page_title(), "Profile")
