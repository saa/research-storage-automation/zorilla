from django.conf import settings
from django.test import TestCase
from zorilla_web.context_processors import *


class ContextProcessorsTestCase(TestCase):
    def test_app_name(self):
        context = app_name(None)
        self.assertIn("app_name", context)
        self.assertEqual(context["app_name"], settings.APP_NAME)
