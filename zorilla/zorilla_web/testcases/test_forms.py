from django.conf import settings
from django.test import TestCase
from zorilla_web.forms import *


class ZFilesystemFormTestCase(TestCase):
    def test_form_validators(self):
        """
        Test various values for snapshot_retention
        """
        for i in range(-128, 128):
            form = ZFilesystemForm(data={"group_id": "42", "snapshot_retention": i})
            form.fields["group_id"].choices = [(42, "forty-two")]
            if i in settings.SNAPSHOT_RETENTION_VALUES:
                self.assertTrue(form.is_valid())
            else:
                self.assertFalse(form.is_valid())
        """
        Test invalid value for group_id
        """
        form = ZFilesystemForm(
            data={
                "group_id": "this is not a number",
                "snapshot_retention": settings.SNAPSHOT_RETENTION_VALUES[0],
            }
        )
        form.fields["group_id"].choices = [(42, "forty-two")]
        self.assertFalse(form.is_valid())


class SelfProvisionFormTestCase(TestCase):
    def test_form_validators(self):
        form = SelfProvisionForm(data={"filesystem_name": "a-ok_", "group_id": 42})
        form.fields["group_id"].choices = [(42, "forty-two")]
        self.assertTrue(form.is_valid())
        form2 = SelfProvisionForm(data={"filesystem_name": "NOT OKAY!", "group_id": 42})
        form2.fields["group_id"].choices = [(42, "forty-two")]
        self.assertFalse(form2.is_valid())
        form3 = SelfProvisionForm(data={"filesystem_name": "$verybad", "group_id": 42})
        form2.fields["group_id"].choices = [(42, "forty-two")]
        self.assertFalse(form2.is_valid())
