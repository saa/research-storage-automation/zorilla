from django.conf import settings
from django.test import TestCase
from zorilla_web.charts import *
from zorilla_core.models import ZFilesystem


class FsQuotaChartTestCase(TestCase):
    def setUp(self):
        self.fs_id = ZFilesystem.objects.create(
            owner_id=1,
            group_id=1,
            fsname="fsname",
            zpool="zpool",
            hostname="hostname",
            used_bytes=1024 ** 3,
            refquota_bytes=1024 ** 3 * 3,
        ).id
        self.chart = FsQuotaChart()

    def test_get_datasets(self):
        data = self.chart.get_datasets(self.fs_id)
        self.assertIsInstance(data, list)
        self.assertEqual(len(data), 1)
        for d in data:
            self.assertIsInstance(d, dict)
            self.assertEqual(d["label"], "fsname")
            self.assertEqual(d["data"], [1.0, 2.0])
            self.assertEqual(d["backgroundColor"], settings.CHART_COLORS)
            self.assertEqual(
                d["hoverBackgroundColor"], settings.CHART_COLORS.copy().reverse()
            )

        self.assertIsNone(self.chart.get_datasets(42))

    def test_get_labels(self):
        self.assertEqual(self.chart.get_labels(), ["Used", "Free"])
