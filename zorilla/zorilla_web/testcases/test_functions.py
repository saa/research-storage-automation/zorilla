from django.conf import settings
from django.contrib.auth.models import User
from django.db.models.query import QuerySet
from django.forms.models import model_to_dict
from django.test import TestCase
from unittest import mock
from zorilla_web.forms import ZFilesystemForm, SelfProvisionForm
from zorilla_web.functions import *
from zorilla_core.models import UserProfile, ZFilesystem, ZSnapshot


class FakeLDAPUser:
    def __init__(self):
        self.uidNumber = 42


class FakeLDAPGroup:
    def __init__(self):
        self.gidNumber = 42
        self.cn = "fortytwo"


class FunctionsTestCase(TestCase):
    def setUp(self):
        self.filesystem = ZFilesystem.objects.create(
            id=1,
            owner_id=42,
            group_id=42,
            fsname="fsname",
            zpool="zpool",
            hostname="hostname",
        )
        self.snapshots = [ZSnapshot.objects.create(filesystem_id=self.filesystem.id)]
        self.user = User.objects.create_user(
            username="test", email="test@example.com", password="test"
        )
        setattr(
            self.user,
            "userprofile",
            UserProfile.objects.create(user=self.user, external_uid="testuser"),
        )

    def test_get_filesystem(self):
        fs = get_filesystem(self.filesystem.id)
        self.assertIsInstance(fs, ZFilesystem)
        self.assertEqual(fs.id, self.filesystem.id)
        self.assertIsNone(get_filesystem(0))

    @mock.patch("zorilla_core.functions.ldap_connection")
    def test_get_grouplist(self, mock_ldap):
        self.assertEqual(["nobody"], get_grouplist(self.user))

    @mock.patch("zorilla_web.functions.lookup_ldap_by_uid")
    def test_get_user_filesystems(self, mock_lookup):
        ldap_user = FakeLDAPUser()
        mock_lookup.return_value = ldap_user
        filesystems = get_user_filesystems(self.user)
        mock_lookup.assert_called_with(
            self.user.userprofile.external_uid, attributes="uidNumber"
        )
        for f in filesystems:
            self.assertEqual(f["owner_id"], str(ldap_user.uidNumber))
            self.assertIsInstance(f["snapshots"], QuerySet)
            for s in f["snapshots"]:
                self.assertEqual(s.filesystem_id, f["id"])

    @mock.patch("zorilla_core.functions.ldap_connection")
    def test_get_filesystem_form(self, mock_ldap):
        form = get_filesystem_form(self.user, get_filesystem(self.filesystem.id))
        self.assertIsInstance(form, ZFilesystemForm)
        self.assertIsInstance(form.fields["group_id"].choices, list)
        self.assertEqual(form.fields["group_id"].initial, str(self.filesystem.group_id))
        self.assertEqual(
            form.fields["snapshot_retention"].initial,
            settings.DEFAULT_SNAPSHOT_RETENTION,
        )

    @mock.patch("zorilla_web.functions.lookup_ldap_by_gid")
    @mock.patch("zorilla_web.functions.lookup_ldap_groups")
    def test_get_group_id_choices(
        self, mock_lookup_ldap_groups, mock_lookup_ldap_by_gid
    ):
        ldap_group = FakeLDAPGroup()
        mock_lookup_ldap_by_gid.return_value = ldap_group
        mock_lookup_ldap_groups.return_value = [ldap_group]

        choices = get_group_id_choices(self.user, initial_choice="42")
        mock_lookup_ldap_groups.assert_called_with(
            self.user.userprofile.external_uid, attributes=["cn", "gidNumber"]
        )
        mock_lookup_ldap_by_gid.assert_called_with("42", attributes=["cn"])
        self.assertEqual(choices, [("42", "fortytwo"), ("99", "nobody")])

    def test_get_fsid_from_path(self):
        self.assertEqual(42, get_fsid_from_path("/filesystem/42/edit/"))

    def test_get_profile(self):
        profile = get_profile(self.user)
        self.assertEqual(profile.user.id, self.user.id)
        self.assertIsInstance(profile, UserProfile)

    def test_set_profile(self):
        temp_user = User()
        self.assertTrue(set_profile(self.user, "self_provisioned", True))
        self.user.userprofile.refresh_from_db()
        self.assertEqual(self.user.userprofile.self_provisioned, True)
        self.assertFalse(set_profile(temp_user, "self_provisioned", True))

    @mock.patch("zorilla_web.functions.mq_publish")
    def test_provision_group_change(self, mock_mq_publish):
        provision_group_change(1, {"group_id": 42})
        mock_mq_publish.assert_called_once()
        mock_mq_publish.assert_called_with(
            {"filesystem_id": 1, "group_id": 42}, routing_key="group_change"
        )

    @mock.patch("zorilla_web.functions.mq_publish")
    def test_provision_backup(self, mock_mq_publish):
        provision_backup(self.user, 1, {"snapshot_retention": 42})
        mock_mq_publish.assert_called_once()
        mock_mq_publish.assert_called_with(
            {"external_uid": "testuser", "filesystem_id": 1, "retention_days": 42},
            routing_key="provision_backup",
        )

    @mock.patch("zorilla_web.functions.lookup_ldap_by_uid")
    @mock.patch("zorilla_web.functions.mq_publish")
    def test_provision_filesystem(self, mock_mq_publish, mock_lookup):
        mock_lookup.return_value = FakeLDAPUser()
        provision_filesystem(
            self.user,
            {
                "filesystem_name": "test",
                "group_id": 42,
                "share_smb": True,
                "share_nfs": False,
            },
        )
        mock_mq_publish.assert_called_once()
        mock_mq_publish.assert_called_with(
            {
                "filesystem_name": "test",
                "owner_id": "42",
                "group_id": 42,
                "quota": int(settings.DEFAULT_FILESYSTEM_QUOTA),
                "refquota": int(settings.DEFAULT_FILESYSTEM_REFQUOTA),
                "zpool": settings.DEFAULT_ZPOOL_NAME,
                "external_uid": "testuser",
                "sharesmb": True,
                "sharenfs": False,
            },
            routing_key="provision_filesystem",
        )

    @mock.patch("zorilla_core.functions.ldap_connection")
    def test_get_self_provision_form(self, mock_ldap):
        form = get_self_provision_form(self.user)
        self.assertIsInstance(form, SelfProvisionForm)
        self.assertEqual(
            form.get_initial_for_field(
                form.fields["filesystem_name"], "filesystem_name"
            ),
            self.user.userprofile.external_uid,
        )
        self.assertEqual(
            form.get_initial_for_field(form.fields["group_id"], "group_id"), "99"
        )
        self.assertEqual(form.fields["group_id"].choices, [("99", "nobody")])
