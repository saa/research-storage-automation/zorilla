from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.test import RequestFactory, TestCase
from unittest import mock
from zorilla_web.middleware import *
from zorilla_core.models import UserProfile


class CustomShibbolethRemoteUserMiddlewareTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username="test", email="test@example.com", password="test"
        )

    def test_make_profile(self):
        shib_meta = {"full_name": "Testy von Testerberger", "external_uid": "ttesterb"}
        middleware = CustomShibbolethRemoteUserMiddleware()
        self.assertFalse(hasattr(self.user, "userprofile"))
        middleware.make_profile(self.user, shib_meta)
        self.assertTrue(hasattr(self.user, "userprofile"))
        self.assertIsInstance(self.user.userprofile, UserProfile)
        self.assertEquals(self.user.userprofile.full_name, shib_meta["full_name"])
        self.assertEquals(self.user.userprofile.external_uid, shib_meta["external_uid"])


class GroupAccessMiddlewareTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.middleware = GroupAccessMiddleware(self.fake_get_response)
        self.user = User.objects.create_user(
            username="test", email="test@example.com", password="test"
        )
        setattr(self.user, "userprofile", UserProfile(user=self.user))

    def fake_get_response(self, request):
        return "fake_response"

    def test___init__(self):
        self.assertTrue(hasattr(self.middleware, "get_response"))

    @mock.patch("zorilla_web.middleware.GroupAccessMiddleware.check_groups")
    @mock.patch("zorilla_web.middleware.resolve")
    def test___call__(self, mock_resolve, mock_get_groups):
        fake_resolve = mock.MagicMock()
        mock_resolve.return_value = fake_resolve

        # this should fail because the app_name is not "zorilla-web"
        fake_resolve.app_name = "zorilla-api"
        request = self.factory.get("/web/")
        response = self.middleware.__call__(request)
        self.assertEqual(response, "fake_response")
        self.assertFalse(hasattr(self.user, "groups_checked"))

        # this has the right app_name, but the user does not pass check_groups
        fake_resolve.app_name = "zorilla-web"
        mock_get_groups.return_value = False
        request = self.factory.get("/web/")
        setattr(request, "user", self.user)
        with self.assertRaises(PermissionDenied):
            self.middleware.__call__(request)

        # this has the right app_name, and the user passes check_groups
        fake_resolve.app_name = "zorilla-web"
        mock_get_groups.return_value = True
        request = self.factory.get("/web/")
        setattr(request, "user", self.user)
        response = self.middleware.__call__(request)
        self.assertEqual(response, "fake_response")
        self.assertTrue(self.user.groups_checked)

    @mock.patch("zorilla_web.middleware.settings")
    @mock.patch("zorilla_web.middleware.get_grouplist", return_value=[])
    def test_check_groups(self, mock_get_grouplist, mock_settings):
        # __all__ lets everyone in
        mock_settings.ALLOWED_GROUPS = ["__all__"]
        self.assertTrue(self.middleware.check_groups(self.user))

        # the user isn't in "testgroup" so this should fail
        mock_settings.ALLOWED_GROUPS = ["testgroup"]
        self.assertFalse(self.middleware.check_groups(self.user))

        # the user is in "testgroup" so this should succeed
        mock_get_grouplist.return_value = ["testgroup"]
        self.assertTrue(self.middleware.check_groups(self.user))
