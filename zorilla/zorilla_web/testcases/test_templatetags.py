from django.contrib.auth.models import User
from django.test import TestCase
from unittest import mock
from zorilla_web.templatetags.ldap_search import *
from zorilla_web.templatetags.unit_conversion import *


class FakeLDAPUser:
    def __init__(self):
        self.uid = "test"


class FakeLDAPGroup:
    def __init__(self):
        self.cn = "fortytwo"


class TemplateTagsTestCase(TestCase):
    @mock.patch("zorilla_web.templatetags.ldap_search.lookup_ldap_by_uidnumber")
    def test_ldap_user_name(self, mock_lookup):
        ldap_user = FakeLDAPUser()
        mock_lookup.return_value = ldap_user
        self.assertEqual(ldap_user_name(42), ldap_user.uid)
        mock_lookup.return_value = None
        self.assertEqual(ldap_user_name(42), None)

    @mock.patch("zorilla_web.templatetags.ldap_search.lookup_ldap_by_gid")
    def test_ldap_group_name(self, mock_lookup):
        ldap_group = FakeLDAPGroup()
        mock_lookup.return_value = ldap_group
        self.assertEqual(ldap_group_name(42), ldap_group.cn)

    def test_bytes_to_kbytes(self):
        self.assertEqual(bytes_to_kbytes(1024), 1)

    def test_bytes_to_mbytes(self):
        self.assertEqual(bytes_to_mbytes(1024 ** 2), 1)

    def test_bytes_to_gbytes(self):
        self.assertEqual(bytes_to_gbytes(1024 ** 3), 1)

    def test_bytes_to_tbytes(self):
        self.assertEqual(bytes_to_tbytes(1024 ** 4), 1)
