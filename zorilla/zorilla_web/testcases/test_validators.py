from django.conf import settings
from django.test import TestCase
from zorilla_web.validators import *


class ValidatorsTestCase(TestCase):
    def setUp(self):
        ZFilesystem.objects.create(fsname="test")

    def test_validate_snapshot_retention(self):
        with self.assertRaises(ValidationError):
            validate_snapshot_retention(-5)
        validate_snapshot_retention(settings.SNAPSHOT_RETENTION_VALUES[0])

    def test_validate_filesystem_name(self):
        with self.assertRaises(ValidationError):
            validate_filesystem_name("test")
        validate_filesystem_name("unused")
