from django.test import TestCase
from django.contrib.auth.models import User
from unittest import mock
from zorilla_core.models import UserProfile, ZFilesystem
from zorilla_core.functions import *


class FakeLDAPUser:
    def __init__(self):
        self.uidNumber = 42


class FunctionsTestCase(TestCase):
    @mock.patch("zorilla_core.functions.ldap3")
    def test_ldap_connection(self, mock_ldap):
        ldap_connection()
        mock_ldap.Connection.assert_called()
        mock_ldap.Server.assert_called()

    @mock.patch("zorilla_core.functions.ldap_connection")
    def test_lookup_ldap_by_gid(self, mock_ldap):
        mock_conn = mock.MagicMock()
        mock_conn.entries = ["test"]
        mock_ldap.return_value = mock_conn

        mock_conn.search.return_value = False
        self.assertIsNone(lookup_ldap_by_gid(42))

        mock_conn.search.return_value = True
        self.assertEqual(lookup_ldap_by_gid(42), mock_conn.entries[0])

    @mock.patch("zorilla_core.functions.ldap_connection")
    def test_lookup_ldap_by_uid(self, mock_ldap):
        mock_conn = mock.MagicMock()
        mock_conn.entries = ["test"]
        mock_ldap.return_value = mock_conn

        mock_conn.search.return_value = False
        self.assertIsNone(lookup_ldap_by_uid("test"))

        mock_conn.search.return_value = True
        self.assertEqual(lookup_ldap_by_uid("test"), mock_conn.entries[0])

    @mock.patch("zorilla_core.functions.ldap_connection")
    def test_lookup_ldap_by_uidnumber(self, mock_ldap):
        mock_conn = mock.MagicMock()
        mock_conn.entries = ["test"]
        mock_ldap.return_value = mock_conn

        mock_conn.search.return_value = False
        self.assertIsNone(lookup_ldap_by_uidnumber(42))

        mock_conn.search.return_value = True
        self.assertEqual(lookup_ldap_by_uidnumber(42), mock_conn.entries[0])

    @mock.patch("zorilla_core.functions.ldap_connection")
    def test_lookup_ldap_groups(self, mock_ldap):
        mock_conn = mock.MagicMock()
        mock_conn.entries = ["test"]
        mock_ldap.return_value = mock_conn

        mock_conn.search.return_value = False
        self.assertEqual(lookup_ldap_groups("test"), [])

        mock_conn.search.return_value = True
        self.assertEqual(lookup_ldap_groups("test"), mock_conn.entries)

    @mock.patch("zorilla_core.functions.settings")
    @mock.patch("zorilla_core.functions.pika", auto_spec=False)
    def test_mq_connect(self, mock_pika, mock_settings):
        mock_settings.RABBITMQ_USER = "test"
        mock_settings.RABBITMQ_PASSWORD = "test"
        mock_settings.RABBITMQ_SERVER = "test"
        mock_settings.RABBITMQ_PORT = 12345
        mock_settings.RABBITMQ_VHOST = "test"
        mock_settings.RABBITMQ_TLS = False
        # test with TLS=False
        channel = mq_connect()
        mock_pika.URLParameters.assert_called_with(
            "amqp://{}:{}@{}:{}/{}".format(
                mock_settings.RABBITMQ_USER,
                mock_settings.RABBITMQ_PASSWORD,
                mock_settings.RABBITMQ_SERVER,
                mock_settings.RABBITMQ_PORT,
                mock_settings.RABBITMQ_VHOST,
            )
        )
        mock_pika.reset_mock()
        # test with TLS=True
        mock_settings.RABBITMQ_TLS = True
        channel = mq_connect()
        mock_pika.URLParameters.assert_called_with(
            "amqps://{}:{}@{}:{}/{}".format(
                mock_settings.RABBITMQ_USER,
                mock_settings.RABBITMQ_PASSWORD,
                mock_settings.RABBITMQ_SERVER,
                mock_settings.RABBITMQ_PORT,
                mock_settings.RABBITMQ_VHOST,
            )
        )
        mock_pika.BlockingConnection.assert_called_once()
        mock_pika.BlockingConnection().channel.assert_called()

    @mock.patch("zorilla_core.functions.time.time", return_value="0123456789.0")
    @mock.patch("zorilla_core.functions.mq_connect")
    def test_mq_publish(self, mock_connect, mock_time):
        mq_publish(
            {"test": "test"},
            routing_key="test",
            exchange="test_exchange",
            queue_arguments={"key": "value"},
        )
        mock_connect().queue_declare.assert_called_with(
            arguments={"key": "value"}, queue="test"
        )
        mock_connect().basic_publish.assert_called_with(
            body='{"test": "test", "timestamp": "0123456789.0"}',
            exchange="test_exchange",
            routing_key="test",
        )
        mock_connect.reset_mock()
        # test with add_timestamp=False
        mq_publish(
            {"test": "test"},
            routing_key="test",
            exchange="test_exchange",
            queue_arguments={"key": "value"},
            add_timestamp=False,
        )
        mock_connect().queue_declare.assert_called_with(
            arguments={"key": "value"}, queue="test"
        )
        mock_connect().basic_publish.assert_called_with(
            body='{"test": "test"}', exchange="test_exchange", routing_key="test"
        )

    @mock.patch("zorilla_core.functions.lookup_ldap_by_uid")
    def test_user_is_owner(self, mock_ldap):
        ldap_user = FakeLDAPUser()
        user = User.objects.create()
        filesystem = ZFilesystem.objects.create(
            fsname="test", owner_id=str(ldap_user.uidNumber)
        )
        # user has no profile
        self.assertFalse(user_is_owner(user, filesystem))
        # user has profile and owns the object
        mock_ldap.return_value = ldap_user
        setattr(user, "userprofile", UserProfile(user=user))
        self.assertTrue(user_is_owner(user, filesystem))
        # user has profile and doesn't own the object
        filesystem2 = ZFilesystem.objects.create(fsname="test2", owner_id=str(-1))
        self.assertFalse(user_is_owner(user, filesystem2))
