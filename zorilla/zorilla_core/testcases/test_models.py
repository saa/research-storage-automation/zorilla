from django.conf import settings
from django.test import TestCase
from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from rest_framework.authtoken.models import Token
from zorilla_core.models import *


class ZorillaCoreModelTestCase(TestCase):
    def test_create_invalid_zfilesystem(self):
        """
        Ensure we can't have two records with the same zpool + fsname
        """
        with self.assertRaises(IntegrityError):
            ZFilesystem.objects.create(
                owner_id=1,
                group_id=1,
                fsname="fsname",
                zpool="zpool",
                hostname="hostname",
            )
            ZFilesystem.objects.create(
                owner_id=1,
                group_id=1,
                fsname="fsname",
                zpool="zpool",
                hostname="hostname",
            )

    def test_create_auth_token(self):
        """
        create_auth_token is a post-save hook on the User model, so we can test it by
        creating a User and seeing if the Token is created
        """
        user = User.objects.create()
        tokens = Token.objects.filter(user_id=user.id)
        self.assertEqual(len(tokens), 1)
