import json, ldap3, logging, pika, time, urllib.parse
from django.conf import settings

logger = logging.getLogger("zorilla.zorilla_core")


def ldap_connection():
    """
    Return an ldap client connection
    """
    server = ldap3.Server(
        settings.LDAP_SERVER,
        port=settings.LDAP_PORT,
        use_ssl=settings.LDAP_TLS,
        get_info=ldap3.ALL,
    )
    return ldap3.Connection(server, auto_bind=True)


def lookup_ldap_by_gid(gid, attributes=[]):
    """
    Look up an ldap user by gid
    """
    conn = ldap_connection()
    if conn.search(
        settings.LDAP_GROUP_SEARCH_BASE,
        "(gidNumber={})".format(gid),
        attributes=attributes,
    ):
        return conn.entries[0]


def lookup_ldap_by_uid(uid, attributes=[]):
    """
    Look up an ldap user by uid
    """
    conn = ldap_connection()
    if conn.search(
        settings.LDAP_USER_SEARCH_BASE, "(uid={})".format(uid), attributes=attributes
    ):
        return conn.entries[0]


def lookup_ldap_by_uidnumber(uidnumber, attributes=[]):
    """
    Look up an ldap user by uid
    """
    conn = ldap_connection()
    if conn.search(
        settings.LDAP_USER_SEARCH_BASE,
        "(uidNumber={})".format(uidnumber),
        attributes=attributes,
    ):
        return conn.entries[0]


def lookup_ldap_groups(uid, attributes=[]):
    """
    Return a list of ldap groups for a given uid
    """
    conn = ldap_connection()
    if conn.search(
        settings.LDAP_GROUP_SEARCH_BASE,
        "(memberUid={})".format(uid),
        attributes=attributes,
    ):
        return conn.entries
    return []


def mq_connect():
    """
    Return a connected RabbitMQ channel
    """
    if settings.RABBITMQ_TLS:
        proto = "amqps"
    else:
        proto = "amqp"
    url = "{}://{}:{}@{}:{}/{}".format(
        proto,
        settings.RABBITMQ_USER,
        settings.RABBITMQ_PASSWORD,
        settings.RABBITMQ_SERVER,
        settings.RABBITMQ_PORT,
        urllib.parse.quote_plus(settings.RABBITMQ_VHOST),
    )
    connection = pika.BlockingConnection(pika.URLParameters(url))
    return connection.channel()


def mq_publish(
    body, exchange="", routing_key="default", add_timestamp=True, queue_arguments={}
):
    """
    Publish to RabbitMQ
    """
    if add_timestamp:
        body["timestamp"] = time.time()
    channel = mq_connect()
    channel.queue_declare(queue=routing_key, arguments=queue_arguments)
    channel.basic_publish(
        exchange=exchange, routing_key=routing_key, body=json.dumps(body)
    )


def user_is_owner(user, obj):
    """
    Return True if user's LDAP uid matches the object's owner_id
    """
    if hasattr(user, "userprofile"):
        ldap_user = lookup_ldap_by_uid(
            user.userprofile.external_uid, attributes=["uidNumber"]
        )
        return str(ldap_user.uidNumber) == obj.owner_id
    else:
        return False
