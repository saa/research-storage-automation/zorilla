from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    """
    Auto-create an API token when a new user object is saved
    """
    if created:
        Token.objects.create(user=instance)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField("Full Name", null=True, max_length=128)
    external_uid = models.CharField(
        "External UID", null=True, max_length=16, unique=True
    )
    self_provisioned = models.BooleanField("Self Provisioned", default=False)


class ZFilesystem(models.Model):
    class Meta:
        unique_together = (("hostname", "zpool", "fsname"),)

    owner_id = models.CharField("Owner", max_length=16)
    group_id = models.CharField("Group", max_length=16)
    fsname = models.CharField("Filesystem Name", max_length=128)
    zpool = models.CharField("ZPool", max_length=128)
    mountpoint = models.CharField("Mountpoint", max_length=128)
    hostname = models.CharField("Hostname", max_length=128)
    sharesmb = models.BooleanField("Share SMB", default=False)
    sharenfs = models.BooleanField("Share NFS", default=False)
    quota_bytes = models.BigIntegerField("Quota", default=0)
    refquota_bytes = models.BigIntegerField("Ref Quota", default=0)
    used_bytes = models.BigIntegerField("Used", default=0)
    update_tag = models.CharField(max_length=24, null=True)
    smb_sharename = models.CharField("SMB Share Name", max_length=128, null=True)
    snapshot_retention = models.IntegerField(
        "Snapshot Retention Period", default=settings.DEFAULT_SNAPSHOT_RETENTION
    )


class ZSnapshot(models.Model):
    filesystem_id = models.IntegerField("Filesystem ID", null=True)
    owner_id = models.CharField("Owner", max_length=16)
    name = models.CharField("Snapshot Name", max_length=128, unique=True)
    update_tag = models.CharField(max_length=24, null=True)
