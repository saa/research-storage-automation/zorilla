# Generated by Django 2.2.6 on 2019-10-17 17:49

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ZSnapshot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('filesystem_id', models.IntegerField(null=True, verbose_name='Filesystem ID')),
                ('owner_id', models.CharField(max_length=16, verbose_name='Owner')),
                ('name', models.CharField(max_length=128, unique=True, verbose_name='Snapshot Name')),
                ('update_tag', models.CharField(max_length=24, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ZFilesystem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('owner_id', models.CharField(max_length=16, verbose_name='Owner')),
                ('group_id', models.CharField(max_length=16, verbose_name='Group')),
                ('fsname', models.CharField(max_length=128, verbose_name='Filesystem Name')),
                ('zpool', models.CharField(max_length=128, verbose_name='ZPool')),
                ('mountpoint', models.CharField(max_length=128, verbose_name='Mountpoint')),
                ('hostname', models.CharField(max_length=128, verbose_name='Hostname')),
                ('sharesmb', models.BooleanField(default=False, verbose_name='Share SMB')),
                ('sharenfs', models.BooleanField(default=False, verbose_name='Share NFS')),
                ('quota_bytes', models.BigIntegerField(default=0, verbose_name='Quota')),
                ('refquota_bytes', models.BigIntegerField(default=0, verbose_name='Ref Quota')),
                ('used_bytes', models.BigIntegerField(default=0, verbose_name='Used')),
                ('update_tag', models.CharField(max_length=24, null=True)),
                ('smb_sharename', models.CharField(max_length=128, null=True, verbose_name='SMB Share Name')),
            ],
            options={
                'unique_together': {('zpool', 'fsname')},
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('full_name', models.CharField(max_length=128, null=True, verbose_name='Full Name')),
                ('external_uid', models.CharField(max_length=16, null=True, unique=True, verbose_name='External UID')),
                ('self_provisioned', models.BooleanField(default=False, verbose_name='Self Provisioned')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
