import logging
from django.test import TestCase

# disable logging during tests
logging.disable(logging.CRITICAL)

# Create your tests here.
