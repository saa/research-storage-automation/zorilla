from django.apps import AppConfig


class ZorillaCoreConfig(AppConfig):
    name = "zorilla_core"
