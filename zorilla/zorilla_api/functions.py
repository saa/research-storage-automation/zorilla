import django.apps, logging
from zorilla_core.functions import mq_publish

logger = logging.getLogger("zorilla.zorilla_api")


def check_database():
    """
    Check that all expected models exist in the db
    """
    try:
        for model in django.apps.apps.get_models(
            include_auto_created=True, include_swapped=True
        ):
            model.objects.exists()
        message = {"ok": True, "message": "OK"}
        logger.debug("Database check: {}".format(message))
        return message
    except Exception as e:
        message = {"ok": False, "message": str(e)}
        logger.error("Database check: {}".format(message))
        return message


def check_mq():
    """
    Send a test message to the messaging queue
    """
    try:
        mq_publish({}, routing_key="healthcheck", queue_arguments={"x-message-ttl": 60})
        message = {"ok": True, "message": "OK"}
        logger.debug("Message queue check: {}".format(message))
        return message
    except Exception as e:
        message = {"ok": False, "message": str(e)}
        logger.error("Message queue check: {}".format(message))
        return message
