from django.test import TestCase
from unittest import mock
from zorilla_api.functions import *


class ZorillaAPIFunctionsTestCase(TestCase):
    @mock.patch("django.apps.apps.get_models")
    def test_check_database(self, mock_get_models):
        self.assertEquals(check_database(), {"ok": True, "message": "OK"})
        mock_get_models.side_effect = Exception
        self.assertFalse(check_database()["ok"])

    @mock.patch("zorilla_api.functions.mq_publish")
    def test_check_mq(self, mock_mq_publish):
        self.assertEquals(check_mq(), {"ok": True, "message": "OK"})
        mock_mq_publish.side_effect = Exception
        self.assertFalse(check_mq()["ok"])


# def check_mq():
#     """
#     Send a test message to the messaging queue
#     """
#     try:
#         mq_publish({}, routing_key="healthcheck", queue_arguments={"x-message-ttl": 60})
#         return {"ok": True, "message": "OK"}
#     except Exception as e:
#         return {"ok": False, "message": str(e)}
