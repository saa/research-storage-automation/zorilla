from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.db.models.query import QuerySet
from django.http import Http404
from django.test import TestCase
from rest_framework.test import APIClient, APIRequestFactory
from unittest import mock
from zorilla_core.models import ZFilesystem, ZSnapshot, UserProfile
from zorilla_api.views import *


class FakeLDAPUser:
    def __init__(self):
        self.uidNumber = 42


class PreFilteredModelViewSetTestCase(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()

    @mock.patch("zorilla_api.views.lookup_ldap_by_uid")
    def test_get_queryset(self, mock_ldap):
        ldap_user = FakeLDAPUser()
        mock_ldap.return_value = ldap_user
        request = self.factory.post("/api/")
        user = User.objects.create()
        setattr(request, "user", user)
        # create two filesystems, one owned by our user and one not
        ZFilesystem.objects.create(fsname="yours", owner_id=ldap_user.uidNumber)
        ZFilesystem.objects.create(fsname="notyours", owner_id=-1)

        # user is not staff and has no userprofile: raise 404
        viewset = PreFilteredModelViewSet(
            request=request, queryset=ZFilesystem.objects.all()
        )
        with self.assertRaises(Http404):
            viewset.get_queryset()

        # user is not staff and has a userprofile, and the request is a write: raise 403
        setattr(request.user, "userprofile", UserProfile(user=user))
        queryset = viewset.get_queryset()
        self.assertIsInstance(queryset, QuerySet)
        self.assertEqual(len(queryset), 1)

        # user is staff: return QuerySet with two results
        setattr(request.user, "is_staff", True)
        queryset = viewset.get_queryset()
        self.assertIsInstance(queryset, QuerySet)
        self.assertEqual(len(queryset), 2)


class AuthenticatedViewSetTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create()
        self.ldap_user = FakeLDAPUser()
        setattr(
            self.user,
            "userprofile",
            UserProfile(user=self.user, external_uid=self.ldap_user.uidNumber),
        )
        self.extraSetup()

    def extraSetup(self):
        pass

    def anonymous_get(self, url=None):
        self.client.force_authenticate(user=None)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)
        return response

    @mock.patch("zorilla_api.views.lookup_ldap_by_uid")
    def get(self, mock_ldap, url=None):
        # test anonymous
        self.client.force_authenticate(user=None)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)
        # test authenticated
        mock_ldap.return_value = self.ldap_user
        self.client.force_authenticate(user=self.user)
        self.user.is_staff = False
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        return response

    @mock.patch("zorilla_api.views.lookup_ldap_by_uid")
    def post(self, mock_ldap, url=None, data=None):
        # test anonymous
        self.client.force_authenticate(user=None)
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 401)
        # test authenticated
        mock_ldap.return_value = self.ldap_user
        self.client.force_authenticate(user=self.user)
        self.user.is_staff = True
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 201)
        return response

    @mock.patch("zorilla_api.views.lookup_ldap_by_uid")
    def patch(self, mock_ldap, url=None, data=None):
        # test anonymous
        self.client.force_authenticate(user=None)
        response = self.client.patch(url, data)
        self.assertEqual(response.status_code, 401)
        # test authenticated
        mock_ldap.return_value = self.ldap_user
        self.client.force_authenticate(user=self.user)
        self.user.is_staff = True
        response = self.client.patch(url, data)
        self.assertEqual(response.status_code, 200)
        return response


class FilesystemViewSetTestCase(AuthenticatedViewSetTestCase):
    def extraSetup(self):
        self.filesystem = ZFilesystem.objects.create(
            fsname="test", owner_id=self.ldap_user.uidNumber
        )

    def test_anonymous_get(self):
        self.anonymous_get(url="/api/filesystems/")

    def test_get(self):
        self.get(url="/api/filesystems/")

    def test_get_by_id(self):
        self.get(url="/api/filesystems/{}/".format(self.filesystem.id))

    def test_post(self):
        self.post(
            url="/api/filesystems/",
            data={
                "owner_id": self.ldap_user.uidNumber,
                "group_id": 42,
                "fsname": "test_filesystem",
                "zpool": "tank",
                "mountpoint": "/tank/test_filesystem",
                "hostname": "example.com",
            },
        )

    def test_patch(self):
        self.patch(
            url="/api/filesystems/{}/".format(self.filesystem.id),
            data={"fsname": "updated_filesystem"},
        )


class SnapshotViewSetTestCase(AuthenticatedViewSetTestCase):
    def extraSetup(self):
        self.snapshot = ZSnapshot.objects.create(
            name="test@snap", owner_id=self.ldap_user.uidNumber
        )

    def test_anonymous_get(self):
        self.anonymous_get(url="/api/snapshots/")

    def test_get(self):
        self.get(url="/api/snapshots/")

    def test_get_by_id(self):
        self.get(url="/api/snapshots/{}/".format(self.snapshot.id))

    def test_post(self):
        self.post(
            url="/api/snapshots/",
            data={"owner_id": self.ldap_user.uidNumber, "name": "test@snap2"},
        )

    def test_patch(self):
        self.patch(
            url="/api/snapshots/{}/".format(self.snapshot.id),
            data={"name": "updated@snap"},
        )


class HealthViewsTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create()

    def test_health_ping(self):
        response = self.client.get("/api/ping/")
        self.assertEqual(response.status_code, 200)

    def test_health_ping(self):
        anon_response = self.client.get("/api/healthcheck/")
        self.assertEqual(anon_response.status_code, 401)

        self.client.force_authenticate(user=self.user)
        auth_response = self.client.get("/api/healthcheck/")
        self.assertEqual(auth_response.status_code, 200)
