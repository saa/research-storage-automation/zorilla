from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import permissions
from rest_framework.test import APIRequestFactory
from zorilla_api.permissions import ReadOnlyUnlessStaff


class PermissionsTestCase(TestCase):
    def test_has_permission(self):
        user = User.objects.create()
        factory = APIRequestFactory()
        perm = ReadOnlyUnlessStaff()
        # safe requests should always succeed
        safe_request = factory.get("/api/")
        self.assertTrue(perm.has_permission(safe_request, None))
        # unsafe requests where is_staff = False should fail
        unsafe_request = factory.post("/api/", {})
        setattr(unsafe_request, "user", user)
        self.assertFalse(perm.has_permission(unsafe_request, None))
        # unsafe requests where is_staff = True should succeed
        setattr(user, "is_staff", True)
        self.assertTrue(perm.has_permission(unsafe_request, None))
