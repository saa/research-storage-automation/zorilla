from rest_framework import permissions
from django.utils.translation import ugettext


class ReadOnlyUnlessStaff(permissions.BasePermission):
    """
    Allow everyone to use safe SAFE_METHODS
    Allow staff users to use unsafe methods
    """

    message = ugettext("Only staff users can create or modify resources.")

    def has_permission(self, request, view):
        return request.method in permissions.SAFE_METHODS or request.user.is_staff
