from django.urls import include, path, re_path
from rest_framework import routers
from zorilla_api import views

router = routers.DefaultRouter()
router.register(r"filesystems", views.FilesystemViewSet)
router.register(r"snapshots", views.SnapshotViewSet)

app_name = "zorilla_api"
urlpatterns = [
    path("", include(router.urls)),
    path(r"healthcheck/", views.health_check),
    path(r"ping/", views.health_ping),
    re_path(r"^(?P<version>(v1))/", include(router.urls)),
    re_path(r"^(?P<version>(v1))/healthcheck/$", views.health_check),
    re_path(r"^(?P<version>(v1))/ping/$", views.health_ping),
]
