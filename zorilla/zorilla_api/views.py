from django.core.exceptions import PermissionDenied
from django.db.models.query import QuerySet
from django.http import Http404
from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated, SAFE_METHODS
from rest_framework.response import Response
from zorilla_api.serializers import FilesystemSerializer, SnapshotSerializer
from zorilla_core.models import ZFilesystem, ZSnapshot
from zorilla_core.functions import lookup_ldap_by_uid
from zorilla_api.functions import check_database, check_mq
from zorilla_api.permissions import ReadOnlyUnlessStaff


class PreFilteredModelViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated & ReadOnlyUnlessStaff]

    def get_queryset(self):
        """
        If the user...
          - is_staff, return the full query set
          - has a profile, filter by owner_id
          - has no profile or is not logged in, raise 404
        First match wins
        """
        # run the parent class' get_queryset()
        queryset = super().get_queryset()
        # if user is_staff, return everything
        if self.request.user.is_staff:
            return queryset
        # elif the user has a profile...
        elif hasattr(self.request.user, "userprofile"):
            ldap_user = lookup_ldap_by_uid(
                self.request.user.userprofile.external_uid, attributes=["uidNumber"]
            )
            return queryset.filter(owner_id=ldap_user.uidNumber)
        # else raise 404 Not Found
        else:
            raise Http404


class FilesystemViewSet(PreFilteredModelViewSet):
    queryset = ZFilesystem.objects.all()
    serializer_class = FilesystemSerializer
    filterset_fields = [f.name for f in ZFilesystem._meta.get_fields()]


class SnapshotViewSet(PreFilteredModelViewSet):
    queryset = ZSnapshot.objects.all()
    serializer_class = SnapshotSerializer
    # mind-bogglingly, the "__all__" setting for filterset_fields does not include "id"!
    filterset_fields = [f.name for f in ZSnapshot._meta.get_fields()]


@api_view()
@permission_classes([AllowAny])
def health_ping(request, version="v1"):
    """
    Unathenticated aliveness check
    """
    db = check_database()
    mq = check_mq()
    if db["ok"] and mq["ok"]:
        return Response({"ping": "OK"})


@api_view()
def health_check(request, version="v1"):
    """
    Do functional tests of components/services
    """
    health = {"database": check_database(), "message_queue": check_mq()}
    return Response({"health": health})
