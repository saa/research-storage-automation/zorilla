from django.apps import AppConfig


class ZorillaApiConfig(AppConfig):
    name = "zorilla_api"
