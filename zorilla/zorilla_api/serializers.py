from rest_framework import serializers
from zorilla_core.models import ZFilesystem, ZSnapshot


class FilesystemSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="api:zfilesystem-detail")

    class Meta:
        model = ZFilesystem
        fields = [f.name for f in ZFilesystem._meta.get_fields()] + ["url"]


class SnapshotSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="api:zsnapshot-detail")

    class Meta:
        model = ZSnapshot
        fields = [f.name for f in ZSnapshot._meta.get_fields()] + ["url"]
